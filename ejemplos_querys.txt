
Estructura de llamada:

<tipo de llamada> {
  <endpoint>(<param1>: <value1>, <param2>: <value2>  ...) {
    <campo retorno1>
    <campo retorno2>
    <campo retorno3>
    ...
  }
}



# graphql-demo: #

query {
  getFilm(name: "Bright Encounters") {
    film_id
    title
    description
    release_year
    rating
  }
}

query {
  getFilmsByActor(actor: "Nick") {
    release_year
    title   
  }
}

mutation {
  changeFilmDescription(name: "Bright Encounters", description: "Mi nueva descripcion") {
    film_id
    title
    description
    release_year
    film_id
    rating
  }
}


# carrusel_db: #

query {
  getBufferOrdersByStatus {
    orden
  }
}

query {
  getContainers {
    contenedor
    descripcion
  }
}

query {
  getBufferOrdersByStatus(status: "buffer") {
    orden
  }
}